public class FizzBuzzGame {
    String print(int number){
        if (number % 3 == 0 && number % 5 == 0) {
            return "FizzBuzz";
        } else if (number % 3 == 0||(number+"").contains("3")) {
            return "Fizz";
        }
        return (number % 5 == 0||(number+"").contains("5")) ? ("Buzz") : (number + "");
    }
}
