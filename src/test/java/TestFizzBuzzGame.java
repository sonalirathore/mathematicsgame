import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class TestFizzBuzzGame {
    FizzBuzzGame fizzBuzzGame=new FizzBuzzGame();

    @Test
    public void testToPrintTheNumberNotAMultipleOfThreeOrFive() {
        int number = 2;
        String expected = "2";

        String actual = fizzBuzzGame.print(number);

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void testPrintFizzWhenNumberIsMultipleOf3() {
        int number = 3;
        String expected = "Fizz";

        String actual = fizzBuzzGame.print(number);

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void testPrintBuzzWhenNumberIsMultipleOf5() {
        int number = 10;
        String expected = "Buzz";

        String actual = fizzBuzzGame.print(number);

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void testPrintFizzBuzzWhenNumberIsMultipleOfBoth3And5() {
        int number = 15;
        String expected = "FizzBuzz";

        String actual = fizzBuzzGame.print(number);

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void testPrintFizzWhenNumberContainsDigit3() {
        int number = 13;
        String expected = "Fizz";

        String actual = fizzBuzzGame.print(number);

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void testPrintFizzWhenNumberContainsDigit5() {
        int number = 52;
        String expected = "Buzz";

        String actual = fizzBuzzGame.print(number);

        assertThat(actual, is(equalTo(expected)));
    }
}